const PropertiesReader = require('properties-reader');
const prop = PropertiesReader('./config/config.properties');
const {
    cargarDB,
    guardarDB
} = require('./dao-dbApi');
const { request } = require('express');

getProperty = (pty) => {
    return prop.get(pty)
}
// variables de ruta config.properties
const rtCargarPersona = getProperty('ruta.rtCargarPersona');
const rtCrearPersona = getProperty('ruta.rtCrearPersona');

let listPersona = [];

const {
    clssPersona
} =  require('../json/clss-estructura');

const crearPersona = (datos) => {
    try {
        listPersona = cargarDB(rtCargarPersona);

        clssPersona.id = datos.respuesta.id;
        clssPersona.nombre = datos.respuesta.nombre;
        clssPersona.apellido = datos.respuesta.apellido;
        clssPersona.rut = datos.respuesta.rut;
        clssPersona.fecha = datos.respuesta.fecha;

        listPersona.push(clssPersona);
        
        guardarDB(rtCrearPersona, listPersona);

        return clssPersona;

    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    crearPersona
}