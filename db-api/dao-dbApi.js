const fs = require('fs');

const guardarDB = (ruta , listaDatos) => {
    let data = JSON.stringify(listaDatos);

    fs.writeFile(ruta, data, (err) => {
        if(err) {
            throw new Error('ERROR: al grabar data');
        }
    });
}

const cargarDB = (ruta) => {
    try {
        console.log('Estoy cargando la data antigua', ruta);
        return listadoPorHacer = require(ruta);   
    } catch (error) {
        console.log('No estoya cargando la data')
        return listadoPorHacer = [];         
    }
}

module.exports = {
    guardarDB,
    cargarDB
}