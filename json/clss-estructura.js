const mensajeJson = {
    codigo: '',
    mensaje: '',
    respuesta: []
}

const clssPersona = {
    id: '',
    nombre: '',
    apellido: '',
    rut: '',
    fecha: ''
}

module.exports = {
    mensajeJson,
    clssPersona
}