// librerias express.
const express = require("express");
const bodyParse = require('body-parser');
const app = express();
// Variable configurables.
const PropertiesReader = require('properties-reader');
const prop = PropertiesReader('./config/config.properties');

getProperty = (pty) => {
    return prop.get(pty)
}
// Mesajeria Json
const {
    mensajeJson
} = require('./json/clss-estructura');
// funcion para cargar una nueva persona a JSON
const {
    crearPersona
} = require('./db-api/clss-persona');

app.use(express.static('public'));
app.use(bodyParse.urlencoded({extended: false}));
app.use(bodyParse.json());

let portPersona = getProperty('puerto.ptCrearPersona');

// Se puede exponer muchos puertos para el mismo servicio
const server = app.listen(portPersona, () => {
    let host =  server.address().address;
    let port =  server.address().port;
    console.log('El servidor está inicializado en el puerto 3000');
    console.log('============================');
    console.log('Host: '+ host);
    console.log('Port: '+ port);
    console.log('============================');
});

const server1 = app.listen(3001, () => {
    let host =  server1.address().address;
    let port =  server1.address().port;
    console.log('El servidor está inicializado en el puerto 3001');
    console.log('============================');
    console.log('Host: '+ host);
    console.log('Port: '+ port);
    console.log('============================');
});

app.post('/persona-post', function(request, response){
    try {
        mensajeJson.codigo = request.body.codigo;
        mensajeJson.mensaje = request.body.descripcion;
        mensajeJson.respuesta = request.body.datos;

        crearPersona(mensajeJson)

        res = mensajeJson;
        response.end(JSON.stringify(res));
    } catch (error) {
        console.log(error);
    }
});
