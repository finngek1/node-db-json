## "Este archivo esta escrito en Markdown"

###### Este proyecto solo es realizado por entretención no se puede aplicar esta lógica en un proyecto profesional ya que tener información de base de datos en un .JSON esta es vulnerable. 

## Aplicacion Para hacer "Base de datos" con JSON.

Para ejecutar la aplicación usar el siguiente comando:

```
 $ npm install
```
Consulta via postman:

 - Url:
```
http://localhost:3000/persona-post
```
o
```
http://localhost:3001/persona-post
```
 - Json:
```
{
    "codigo": 100,
    "descripcion": "prueba postman",
    "datos": {
        "id": 1,
        "nombre": "Hola",
        "apellido": "Mundo",
        "rut": "11222333k",
        "fecha": "20200706"
    }
}
```

## Instalación de "express":

```
$ npm install --save express
```

 - importar en su proyecto:

```
const express = require("express");
const bodyParse = require('body-parser');
const app = express();
```
 - implemtación de puertos de la api:
```
app.use(express.static('public'));
app.use(bodyParse.urlencoded({extended: false}));
app.use(bodyParse.json());

let port = 3000;

// se genera una Constante para gardar la infomación de lanzamiento de la api
const server = app.listen(port, () => {
    let host =  server.address().address; // se obtiene la IP donde vive la API, si es localhost el resultado es "::"
    let port =  server.address().port; // se obtiene la PUERTO asignado en la variable "port" 
    console.log('El servidor está inicializado:);
    console.log('============================');
    console.log('Host: '+ host);
    console.log('Port: '+ port);
    console.log('============================');
});
```